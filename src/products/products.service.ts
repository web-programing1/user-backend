import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'Apple God', price: 9999 },
  { id: 2, name: 'Normal Milk', price: 100 },
  { id: 3, name: 'Easy Milk', price: 200 },
];

let lastProductId = 4;

@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto,
    };
    products.push(newProduct);
    console.log(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log(users[index]);
    // console.log(updateUserDto);
    const updateProdcut: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProdcut;
    return updateProdcut;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct = products[index];
    products.splice(index, 1);
    return deletedProduct;
  }

  reset() {
    products = [
      { id: 1, name: 'Apple God', price: 9999 },
      { id: 2, name: 'Normal Milk', price: 100 },
      { id: 3, name: 'Easy Milk', price: 200 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
